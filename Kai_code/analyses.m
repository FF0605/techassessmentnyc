load data.mat

%% distribution number of passengers
figure;
passenger = histogram(passenger_count);

xlim([passenger.BinEdges(1) passenger.BinEdges(end)])
barstrings = [num2str(passenger.Values')];
% Create text objects at each location
text([[passenger.BinEdges(1)+0.5]:[passenger.BinEdges(end)-0.5]],passenger.Values,barstrings,'horizontalalignment','center','verticalalignment','bottom')

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(passenger_count))     % Since Data Vector Is (1x25), Multiply �yt� By 4 To Get Percent
ylabel('Percentage')
ylim([0 800000])
xlabel('# passengers per trip')  % add title

title({'Distribution of # passengers per trip'})  % add title

%% distribution of payment methods
figure;
p_type = categorical(payment_type);
payment = histogram(p_type);
% Create strings for each bar count
barstrings = num2str(payment.Values');
% Create text objects at each location
text([1:length(payment.BinCounts)],payment.Values,barstrings,'horizontalalignment','center','verticalalignment','bottom')

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(payment_type))  
ylabel('Percentage')
xlabel('Types of payment methods')  % add title
title({'Distribution of payment methods'})  % add title

%% distribution of fare amount
figure;
fare = histogram(round(fare_amount),'BinWidth',1,'FaceColor','b','EdgeColor','none','FaceAlpha',1);
set(gca,'YScale','log');

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(payment_type))  
ylim([0 1000000])
ylabel('Percentage in logarithm scale')
xlabel('$ fare amount')  % add title
title({'Distribution of fare amount'})  % add title

[pks,locs]=findpeaks(fare.BinCounts,'MinPeakDistance',25);
text(locs, pks, num2str(locs'));

fare_stats = [max(fare_amount) min(fare_amount) round(100*mean(fare_amount))/100];
text(250,90000,['Max:   $' num2str(fare_stats(1))],'Color','red','FontSize',12)
text(250,30000,['Min:    $' num2str(fare_stats(2))],'Color','red','FontSize',12)
text(250,10000,['Mean: $' num2str(fare_stats(3))],'Color','red','FontSize',12)


%% distribution of tip amount
figure;
tip = histogram(tip_amount,'BinWidth',1,'FaceColor','b','EdgeColor','none','FaceAlpha',1);
set(gca,'YScale','log');

[pks,locs]=findpeaks(tip.BinCounts,'MinPeakDistance',8);
tip_txt=num2str(locs'-1);
text(locs, pks, tip_txt);

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(tip_amount))  
ylim([0 1000000])
ylabel('Percentage in logarithm scale')
xlabel('$ tip amount')  % add title
title({'Distribution of tip amount'})  % add title

tip_stats = [max(tip_amount) min(tip_amount) round(100*mean(tip_amount))/100];
text(120,40000,['Max:   $' num2str(tip_stats(1))],'Color','red','FontSize',12)
text(120,19800,['Min:    $' num2str(tip_stats(2))],'Color','red','FontSize',12)
text(120,10000,['Mean: $' num2str(tip_stats(3))],'Color','red','FontSize',12)

%% distribution of total amount
ttl_amount = histogram(total_amount,'BinWidth',1,'FaceColor','b','EdgeColor','none','FaceAlpha',1);
set(gca,'YScale','log');

[pks,locs]=findpeaks(ttl_amount.BinCounts,'MinPeakDistance',40);
total_txt = num2str(locs');
text(locs, pks, total_txt);

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(total_amount))  
ylim([0 1000000])
ylabel('Percentage in logarithm scale')
xlabel('$ total amount')  % add title
title({'Distribution of total amount'})  % add title

total_stats = [max(total_amount) min(total_amount) round(100*mean(total_amount))/100];
text(300,16000,['Max:   $' num2str(total_stats(1))],'Color','red','FontSize',12)
text(300,8000,['Min:    $' num2str(total_stats(2))],'Color','red','FontSize',12)
text(300,4000,['Mean: $' num2str(total_stats(3))],'Color','red','FontSize',12)

%% distribution of top busiest hours of the day
t_pickup = datetime(pickup_datetime);
edges = -0.5:23.5;
histogram(t_pickup.Hour,edges);
%[~,index] = sort(pickup_datetime);
xlim ([-0.5 23.5])
ax = gca;
ax.XTick = 0:2:23;
ax.XTickLabel = {'Midnight','2','4','6','8','10','Noon','14','16','18','20','22'};
ax.XTickLabelRotation = 45;

% convert display numbers to percentages
yt = get(gca, 'YTick'); 
set(gca, 'YTick',yt, 'YTickLabel',yt/length(pickup_datetime))  
ylim([0 70000])
ylabel('Percentage')
xlabel('Time of a day in average')  % add title
title({'Distribution of taxi business in each hour'})  % add title

%% k-means cluster of the busiest locations (incl. pickup and dropoff)
lon_gps = [-74.1678 -73.7317];
lat_gps = [40.5627 40.8966];

% identify pickup locations ouside NYC/invalid
pickup_data = [pickup_latitude,pickup_longitude];
pickup_outlier = (pickup_data(:,1)<lat_gps(1) ...
    |pickup_data(:,1)>lat_gps(2) ...
    |pickup_data(:,2)<lon_gps(1) ...
    |pickup_data(:,2)>lon_gps(2));  

% prepare pickup data for clustering
pickup_c_data = pickup_data;
pickup_c_data(pickup_outlier,:)=[];

% identify dropoff locations ouside NYC/invalid
dropoff_data = [dropoff_latitude,dropoff_longitude];
dropoff_outlier = (dropoff_data(:,1)<lat_gps(1) ...
    |dropoff_data(:,1)>lat_gps(2) ...
    |dropoff_data(:,2)<lon_gps(1) ...
    |dropoff_data(:,2)>lon_gps(2)); 

% prepare dropoff data for clustering
dropoff_c_data = dropoff_data;
dropoff_c_data(dropoff_outlier,:)=[]; 

cluster_data = [pickup_c_data; dropoff_c_data];

n_clusters=50;
[cluster_idx,cluster_ctrd] = kmeans(cluster_data, n_clusters, 'MaxIter',500);


%% cluster of the top 10 busiest areas (2)

% https://maps.googleapis.com/maps/api/staticmap?center=40.7300,-73.950&zoom=11&size=1000x1000&key=AIzaSyBVXNifcrpgFCa3zjYNFrUIQ5qkyLsXFAw
% https://maps.googleapis.com/maps/api/staticmap?center=40.7300,-73.950&zoom=11&size=1000x1000&maptype=satellite&key=AIzaSyBVXNifcrpgFCa3zjYNFrUIQ5qkyLsXFAw

% defined the range of the loaded image
lon_gps = [-74.1678 -73.7317];
lat_gps = [40.5627 40.8966];

figure;
ori_map = imread('googlesat.jpg');  % load the map image
map = imresize(ori_map,3);          % increase image resolution
map = flipud(map);                  % covert coordinate to xy plot
imagesc(lon_gps, lat_gps, map);     % fit the image into the background
hold on;

% population (density) in each cluster
dens_clu = histc(cluster_idx,1:n_clusters);
% convert the location density to circle radius
freq_to_rad = dens_clu/400;
% convert the location density to color transition from yellow to red
loc_colorgrad = [ones(n_clusters,1), 1-dens_clu./max(dens_clu), zeros(n_clusters,1)];

scatter(cluster_data(:,2), cluster_data(:,1), 1, 'MarkerEdgeColor', 'g') % plot data points
daspect([1 1 1]);
set(gca,'ydir','normal'); 

% display top 10 busiest location in an descent order
[~,rank_loc] = sort(dens_clu,'descend');
cluster_ctrd_ranked = cluster_ctrd(rank_loc,:);
% Animation: display the clusters from the busiest to the quietest
pause(2);
for i_loc = 1:10%n_clusters
    scatter(cluster_ctrd_ranked(i_loc,2),cluster_ctrd_ranked(i_loc,1), ...
        freq_to_rad(rank_loc(i_loc)), ...       % sized according to
        loc_colorgrad(rank_loc(i_loc),:), ...   % colored
        'filled', 'MarkerFaceAlpha', 1);
    pause(0.1);
    % get the frames
    frame = getframe;
    im{i_loc} = frame2im(frame);
end

%% save the above animation into a tif file
filename = 'busylocAnimated.gif'; % Specify the output file name
for idx = 1:10
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.5);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.5);
    end
end
%% The busiest 10 trips between clustered locations

n_trips = 10;
% calibrate the location data
location_data = zeros(size(pickup_data));

% identify cluster ID for each location
location_data(~pickup_outlier,1) = cluster_idx(1:length(pickup_c_data));
location_data(~dropoff_outlier,2) = cluster_idx((length(pickup_c_data)+1):end);

% calibrate the trip data
trip_data = location_data;
% remove outlier trips
trip_data((location_data(:,1)==0|location_data(:,2)==0),:)=[];
% re-number the clusters according to its busy level (1->max, 50->min)
trip_data_ranked = changem(trip_data,(1:n_clusters),rank_loc);
cluster_ctrd_ranked = cluster_ctrd(rank_loc,:);

figure;
% define a colormap
colormap autumn
% flip the map, so red means more intensive, yellow means less.
colormap(flipud(colormap))
% display a 3D histogram between all locations
h = histogram2(trip_data_ranked(:,1),trip_data_ranked(:,2),'FaceColor','flat');
% display a color-index bar
colorbar
% keep x y axis the same scale
daspect([1 1 100]);
% set limit for x y axis
xlim([h.XBinEdges(1) h.XBinEdges(end)]);
ylim([h.YBinEdges(1) h.YBinEdges(end)]);
% Sort the trip-counts in descending order
[~,rank_trip] = sort(h.BinCounts(:),'descend');  
[from_,to_] = ind2sub(size(h.BinCounts),rank_trip(1:n_trips)); %The top ten trips

% Display the busiest trips on the map
figure;
% re-draw the map with clusters
ori_map = imread('googlemap.jpg');
map = imresize(ori_map,3);
map = flipud(map);
imagesc(lon_gps, lat_gps, map);
hold on
% display all clusters
scatter(cluster_ctrd(:,2),cluster_ctrd(:,1), freq_to_rad, loc_colorgrad, ...
     'filled', 'MarkerFaceAlpha', 1, 'MarkerEdgeColor', 'black')        % color by count
daspect([1 1 1]);
set(gca,'ydir','normal'); 
hold on
pause(3)
frame = getframe;
im0 = frame2im(frame);  
% zoom in
xlim([-74.05 -73.9]);
ylim([40.68 40.83]);              
drawArrow = @(x,y,varargin) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0, varargin{:});  
% create an animation
for i_trip=1:n_trips

    trip_from = fliplr(cluster_ctrd_ranked(from_(i_trip),:));
    trip_to = fliplr(cluster_ctrd_ranked(to_(i_trip),:));
    
    arr_=drawArrow([trip_from(1) trip_to(1)], [trip_from(2) trip_to(2)],'linewidth',2,'MaxHeadSize',3,'Color', 'black');
    % if pickup and dropoff are within the same cluster, draw a black circle
    if (trip_from==trip_to)
        arr_=scatter(trip_from(1),trip_from(2), 60, 'black', 'filled', 'MarkerFaceAlpha', 1);
    end
    pause(1);
    % get the frames
    frame = getframe;
    im{i_trip} = frame2im(frame);    
    delete(arr_);
end
% generate the busiesr trip in descent order
Top_trip_ID = [from_ to_];

%% save the above animation into a tif file
filename = 'busytripAnimated.gif'; % Specify the output file name
for idx = 1:10
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',1);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',1);
    end
end

%% The most consistent fare
trip_fare = [location_data,fare_amount];
% remove outlier trips
trip_fare((location_data(:,1)==0|location_data(:,2)==0),:)=[];
% find unique trips
[trip_unique, ~, trip_id] = unique(trip_fare(:,1:2),'rows');
% display trips between locations and the corresponding std 
[mean_ std_ length_]=grpstats(trip_fare(:,3), trip_id, {'mean','std',@length});
fare_info = [trip_unique mean_ std_ length_];
% set the minmimum criterion that only trips over 200 records are considered
std_(length_<200)=inf;
[~,rank_f]=sort(std_,'ascend');
consistent_fare = fare_info(rank_f,:);


% plot the top 10 consistent fare
figure;
% re-draw the map with clusters
ori_map = imread('googlemap.jpg');
map = imresize(ori_map,3);
map = flipud(map);
imagesc(lon_gps, lat_gps, map);
hold on
% display all clusters
scatter(cluster_ctrd(:,2),cluster_ctrd(:,1), freq_to_rad, loc_colorgrad, 'filled', 'MarkerFaceAlpha', 1)        % color by count
daspect([1 1 1]);
set(gca,'ydir','normal'); 
hold on
pause(3)
drawArrow = @(x,y,varargin) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0, varargin{:});  
% create an animation shows the first 10 consistent trips
for i_consist=1:10
    trip_from = fliplr(cluster_ctrd(consistent_fare(i_consist,1),:));
    trip_to = fliplr(cluster_ctrd(consistent_fare(i_consist,2),:));    
    arr_=drawArrow([trip_from(1) trip_to(1)], [trip_from(2) trip_to(2)],'linewidth',2,'MaxHeadSize',3,'Color', 'black');
    pause(1);
    
    % get the frames
    frame = getframe;
    im{i_consist} = frame2im(frame);   
    
    delete(arr_);
end
%% save the above animation into a tif file
filename = 'mostconsistAnimated.gif'; % Specify the output file name
for idx = 1:10
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',1);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',1);
    end
end
%% the highest standard deviation of travel time

trip_time = [location_data,trip_time_in_secs];
% remove outlier trips
trip_time((location_data(:,1)==0|location_data(:,2)==0),:)=[];
% find unique trips
[trip_unique, ~, trip_id] = unique(trip_time(:,1:2),'rows');
% display time between locations and the corresponding mean/std/ 
[mean_ std_ length_]=grpstats(trip_time(:,3), trip_id, {'mean','std',@length});
time_info = [trip_unique mean_ std_ length_];

% opt1: find trips with highest std trip time
% set the minmimum criterion that only trips over 200 records are considered
std_(length_<200)=0;
[~,rank_f]=sort(std_,'descend');
consistency_time = time_info(rank_f,:);

% % opt2: find trips with most confident trip time
% % set the minmimum criterion that only trips over 200 records are considered
% std_(length_<200)=inf;
% [~,rank_f]=sort(std_,'ascend');
% consistency_time = time_info(rank_f,:);

% plot the most inconsistent time
figure;
% re-draw the map with clusters
ori_map = imread('googlemap.jpg');
map = imresize(ori_map,3);
map = flipud(map);
imagesc(lon_gps, lat_gps, map);
hold on
% display all clusters
scatter(cluster_ctrd(:,2),cluster_ctrd(:,1), freq_to_rad, loc_colorgrad, 'filled', 'MarkerFaceAlpha', 1)        % color by count
daspect([1 1 1]);
set(gca,'ydir','normal'); 
hold on
pause(3)

% zoom in
xlim([-74.05 -73.9]);
ylim([40.68 40.83]);  
drawArrow = @(x,y,varargin) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0, varargin{:});  
% create an animation shows the first 10 consistent trips
for i_time=1:50
    trip_from = fliplr(cluster_ctrd(consistency_time(i_time,1),:));
    trip_to = fliplr(cluster_ctrd(consistency_time(i_time,2),:));    
    arr_=drawArrow([trip_from(1) trip_to(1)], [trip_from(2) trip_to(2)],'linewidth',1,'Color', 'blue');
    pause(0.5);
    
    % get the frames
    frame = getframe;
    im{i_time} = frame2im(frame);   
    
    %delete(arr_);
end
%% save the above animation into a tif file
filename = 'timelostdAnimated.gif'; % Specify the output file name
for idx = 1:50
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.2);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
end

%% average pickup frequency over time in a day

% defined the range of the loaded image
lon_gps = [-74.1678 -73.7317];
lat_gps = [40.5627 40.8966];

figure;
ori_map = imread('googlesat.jpg');  % load the map image
map = imresize(ori_map,3);          % increase image resolution
map = flipud(map);                  % covert coordinate to xy plot
imagesc(lon_gps, lat_gps, map);     % fit the image into the background
hold on;

nbins = 150;                                            % number of bins
xedges = linspace(lon_gps(1),lon_gps(2),nbins);      % x-axis bin edges
yedges = linspace(lat_gps(1),lat_gps(2),nbins);      % y-axis bin edges

% calibrate the location data
time_pickup = pickup_datetime;
% remove outliers/locations outside NYC
time_pickup(pickup_outlier) = [];
% read time
t_pickup = datetime(time_pickup);

pause(3);
for idx_hour = 0:23
    colormap autumn                     % define a colormap
    colormap(flipud(colormap))          % flip the colormap
    % create a hist heat map
    temp_hist = histogram2(pickup_c_data(t_pickup.Hour==idx_hour,2), ...
        pickup_c_data(t_pickup.Hour==idx_hour,1), ...
        xedges, yedges, ...             % overlay histogram
        'DisplayStyle', 'tile', ...     % in 2D style
        'FaceAlpha', 0.5)
    daspect([1 1 1]);
    set(gca,'ydir','normal');           % flip back to normal
    caxis([0 300])                      % color axis scaling
    title({'NYC Taxi Pickup Frequency in a Day'; '1st Apr - 30th Apr 2013'})  % add title
    colorbar
    % add time info on the map
    temp_text = text(-74.14,40.85, ...
        ['Time: ' num2str(idx_hour) ':00 - ' num2str(idx_hour+1) ':00'], ...
        'Color','white', ...
        'FontSize',20, ...
        'FontWeight','bold')
    %pause(0.1)

    % get the frames
    frame = getframe;
    im{idx_hour+1} = frame2im(frame);   
    
    delete(temp_hist)
    delete(temp_text)
end
%% save the above animation into a tif file
filename = 'hourintensityAnimated.gif'; % Specify the output file name
for idx = 1:24
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.4);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.4);
    end
end   